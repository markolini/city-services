package hr.com.city.service.impl;

import hr.com.city.dao.UserDao;
import hr.com.city.domain.UserAccount;
import hr.com.city.errors.exceptions.UserAlreadyExistsException;
import hr.com.city.service.UserService;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * User services business logic implementation layer.
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private UserDao userRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserDao userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void registerUser(UserAccount userAccount) {
        Optional<UserAccount> userAccountOptional = userRepository.findByEmail(userAccount.getEmail());

        if (userAccountOptional.isPresent()) {
            log.error("User {} already exists", userAccount.getEmail());
            throw new UserAlreadyExistsException("User already exists");
        }

        userAccount.setPassword(passwordEncoder.encode(userAccount.getPassword()));
        userRepository.save(userAccount);
        log.info("User {} successfully created", userAccount.getEmail());
    }
}
