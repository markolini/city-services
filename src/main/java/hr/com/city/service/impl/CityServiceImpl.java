package hr.com.city.service.impl;

import hr.com.city.dao.CityDao;
import hr.com.city.dao.FavoritesDao;
import hr.com.city.domain.City;
import hr.com.city.domain.Favorites;
import hr.com.city.errors.exceptions.CityAlreadyExistsException;
import hr.com.city.errors.exceptions.CityDoesNotExistException;
import hr.com.city.errors.exceptions.FavoritesEmptyException;
import hr.com.city.service.CityService;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * City services business logic implementation layer.
 */
@Service
@Slf4j
public class CityServiceImpl implements CityService {

    private CityDao cityRepository;
    private FavoritesDao favoritesRepository;

    @Autowired
    public CityServiceImpl(CityDao cityRepository, FavoritesDao favoritesRepository) {
        this.cityRepository = cityRepository;
        this.favoritesRepository = favoritesRepository;
    }

    @Override
    public void createCity(City city) {
        Optional<City> optionalCity = cityRepository.findByName(city.getName());

        if (optionalCity.isPresent()) {
            log.error("City already exists inside the database created {}", city.getName());
            throw new CityAlreadyExistsException("City already exists");
        } else {
            city.setCreated(new Date());
            city.setFavoriteCount(0);
            cityRepository.save(city);
            log.info("City successfully created {}", city.getName());
        }
    }

    @Override
    public void addCityToFavorites(String email, String cityName) {
        Favorites favoriteCity = new Favorites(email, cityName);
        Optional<List<Favorites>> optionalFavorites = favoritesRepository.findByEmail(email);

        if (optionalFavorites.isPresent()) {
            List<Favorites> favoriteCities = optionalFavorites.get();

            if (favoriteCities.contains(favoriteCity)) {
                log.error("City {} is already located in the users {} favorites", cityName, email);
                throw new CityAlreadyExistsException("City already located in favorites");
            } else {
                favoritesRepository.save(favoriteCity);
                log.info("City {} is successfully saved to users {} favorites", cityName, email);
            }
        } else {
            favoritesRepository.save(favoriteCity);
            log.info("City {} is successfully saved to users {} favorites", cityName, email);
        }
    }

    @Override
    public void removeCityFromFavorites(String email, String cityName) {
        Favorites cityToRemove = new Favorites(email, cityName);
        Optional<List<Favorites>> optionalFavorites = favoritesRepository.findByEmail(email);

        if (optionalFavorites.isPresent()) {
            List<Favorites> favoriteCities = optionalFavorites.get();

            if (favoriteCities.contains(cityToRemove)) {
                Favorites favoriteToRemove = favoriteCities.stream()
                        .filter(favorite -> favorite.equals(cityToRemove)).findAny().get();

                favoritesRepository.delete(favoriteToRemove);
                log.info("City {} is successfully deleted from the users {} favorites", cityName, email);
            } else {
                log.error("City {} is NOT located in the users {} favorites", cityName, email);
                throw new CityDoesNotExistException("City not in the users favorites");
            }
        } else {
            log.error("User {} has no favorites", email);
            throw new FavoritesEmptyException("User has no favorite cities");
        }
    }

    @Override
    public Optional<List<City>> fetchAllCities() {
        List<City> cities = cityRepository.findAll();

        if (noCitiesFound(cities)) {
            return Optional.empty();
        }

        return Optional.of(cities);
    }

    @Override
    public Optional<List<City>> fetchAllCitiesSortedByDate() {
        List<City> cities = cityRepository.findAll();

        if (noCitiesFound(cities)) {
            return Optional.empty();
        }

        cities.sort(Comparator.comparing(City::getCreated).reversed());

        return Optional.of(cities);
    }

    @Override
    public Optional<List<City>> fetchAllCitiesByFavorites() {
        List<City> cities = cityRepository.findAll();

        if (noCitiesFound(cities)) {
            return Optional.empty();
        }

        cities.sort(Comparator.comparing(City::getFavoriteCount).reversed());

        return Optional.of(cities);
    }

    private boolean noCitiesFound(List<City> cities) {
        return cities.isEmpty();
    }
}
