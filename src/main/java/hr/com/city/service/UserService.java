package hr.com.city.service;

import hr.com.city.domain.UserAccount;

/**
 * User service business logic layer.
 */
public interface UserService {

    /**
     * Creates the given user account account in the database. Before saving, the password will be encrypted. If a
     * userAccount account with the same email already exists, throws {@link hr.com.city.errors.exceptions.UserAlreadyExistsException}
     *
     * @param userAccount {@link UserAccount} to create in database
     */
    public void registerUser(UserAccount userAccount);
}
