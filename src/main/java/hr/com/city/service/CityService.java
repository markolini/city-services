package hr.com.city.service;

import hr.com.city.domain.City;
import java.util.List;
import java.util.Optional;

/**
 * City service business logic layer.
 */
public interface CityService {

    /**
     * Adds a new city to the database. Throws {@link hr.com.city.errors.exceptions.CityAlreadyExistsException} if the
     * city is already in the database.
     *
     * @param city {@link City} to create in database
     */
    public void createCity(City city);

    /**
     * Adds a new city to the users favorites. Throws {@link hr.com.city.errors.exceptions.CityAlreadyExistsException}
     * if the user already has the city in his favorites list.
     *
     * @param email {@link String} that adds the city to its favorites
     * @param cityName {@link String} to add to favorites
     */
    public void addCityToFavorites(String email, String cityName);

    /**
     * Removes a city that is in the users favorites. Throws {@link hr.com.city.errors.exceptions.CityDoesNotExistException}
     * if the given city is not in the users favorites list. Also throws {@link hr.com.city.errors.exceptions.FavoritesEmptyException}
     * if the user has an emtpy favorites list.
     *
     * @param email {@link String} that adds the city to its favorites
     * @param cityName {@link String} to be removed to favorites
     */
    public void removeCityFromFavorites(String email, String cityName);

    /**
     * Fetches all cities from the database, unsorted.
     *
     * @return optional {@link List} of {@link City}
     */
    public Optional<List<City>> fetchAllCities();

    /**
     * Fetches all cities from the database, sorted by creating date.
     *
     * @return optional {@link List} of {@link City}
     */
    public Optional<List<City>> fetchAllCitiesSortedByDate();

    /**
     * Fetches all cities from the database, sorted by favoritism.
     *
     * @return optional {@link List} of {@link City}
     */
    public Optional<List<City>> fetchAllCitiesByFavorites();
}
