package hr.com.city.security;

import static java.util.Collections.emptyList;

import hr.com.city.dao.UserDao;
import hr.com.city.domain.UserAccount;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Service used for user authentication during the JWT exchange.
 */
@Service
@Slf4j
class UserAuthenticationService implements UserDetailsService {

    private UserDao userRepository;

    @Autowired
    UserAuthenticationService(UserDao userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<UserAccount> applicationUser = userRepository.findByEmail(email);

        if (applicationUser.isPresent() == false) {
            log.error("User {} not found", email);
            throw new UsernameNotFoundException(email);
        }

        UserAccount user = applicationUser.get();

        return new User(user.getEmail(), user.getPassword(), emptyList());
    }
}
