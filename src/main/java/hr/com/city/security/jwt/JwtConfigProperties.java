package hr.com.city.security.jwt;

/**
 * JWT configuration constants. Header key and token prefix.
 */
public class JwtConfigProperties {

    static final String JWT_HEADER_KEY = "Authorization";
    static final String JWT_TOKEN_PREFIX = "Bearer ";
}
