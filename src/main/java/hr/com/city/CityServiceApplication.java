package hr.com.city;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This is the main class for the City Service application. REST API documentation exposed with Swagger.
 *
 * @author Marko Kos
 * @version 1.0.0
 * @see <a href="http://localhost:8080/swagger-ui.html"> Swagger 2 API</a>
 */
@SpringBootApplication
public class CityServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CityServiceApplication.class, args);
    }
}
