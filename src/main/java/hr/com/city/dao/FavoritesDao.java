package hr.com.city.dao;

import hr.com.city.domain.Favorites;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Favorites repository with all the needed favorites methods.
 */
@Repository
public interface FavoritesDao extends JpaRepository<Favorites, Long> {

    /**
     * Fetches a list of favorite cities for a given user.
     *
     * @param email {@link String} of the user
     * @return optional {@link List} of {@link Favorites} object
     */
    Optional<List<Favorites>> findByEmail(String email);
}
