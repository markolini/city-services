package hr.com.city.dao;

import hr.com.city.domain.UserAccount;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * User repository with all the needed user methods.
 */
@Repository
public interface UserDao extends JpaRepository<UserAccount, Long> {

    /**
     * Fetches a user account for a given email.
     *
     * @param email {@link String} of the user
     * @return optional {@link UserAccount} object
     */
    Optional<UserAccount> findByEmail(String email);
}
