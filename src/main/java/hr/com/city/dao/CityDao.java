package hr.com.city.dao;

import hr.com.city.domain.City;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * City repository with all the needed city methods.
 */
@Repository
public interface CityDao extends JpaRepository<City, Long> {

    /**
     * Fetches a city from the database by its name if found.
     *
     * @param name {@link String} name
     * @return optional {@link City} object
     */
    Optional<City> findByName(String name);
}
