package hr.com.city.triggers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.h2.api.Trigger;
import org.springframework.stereotype.Component;

/**
 * Database trigger for decrementing a city favorite counter. The idea is to keep track how many users has a certain
 * city put as a favorite. If removed from the favorites, this trigger decrements the favorite city value.
 */
@Component
public class DecrementTrigger implements Trigger {

    @Override
    public void init(Connection conn, String schemaName, String triggerName, String tableName, boolean before, int type)
            throws SQLException {
    }

    @Override
    public void fire(Connection conn, Object[] oldRow, Object[] newRow) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(
                "UPDATE cities SET cts_favorite_counter = cts_favorite_counter - 1 WHERE cts_name = ?")
        ) {
            stmt.setObject(1, oldRow[2]);
            stmt.executeUpdate();
        }
    }

    @Override
    public void close() throws SQLException {
    }

    @Override
    public void remove() throws SQLException {
    }
}
