package hr.com.city.api;

import hr.com.city.domain.UserAccount;
import hr.com.city.errors.exceptions.ExceptionResponse;
import hr.com.city.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * User REST API endpoint layer with all the necessary user services exposed.
 */
@RestController
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(path = "/register")
    @ApiOperation("Register a userAccount for given data.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request", response = ExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
            @ApiResponse(code = 409, message = "Conflict", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ExceptionResponse.class)})
    public ResponseEntity registerUser(@RequestBody UserAccount userAccount) {
        userService.registerUser(userAccount);
        return ResponseEntity.ok().build();
    }

    /* A dummy method for Swagger display. Logging is handled by the JWT component. */
    @PostMapping(path = "/login")
    @ApiOperation("User account login.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request", response = ExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ExceptionResponse.class)})
    public ResponseEntity login(@RequestBody UserAccount userAccount) {
        return ResponseEntity.ok().build();
    }
}
