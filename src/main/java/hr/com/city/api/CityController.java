package hr.com.city.api;

import hr.com.city.domain.City;
import hr.com.city.errors.exceptions.ExceptionResponse;
import hr.com.city.service.CityService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * City REST API endpoint layer with all the necessary city services exposed.
 */
@RestController
public class CityController {

    private CityService cityService;

    @Autowired
    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @PostMapping(path = "/city")
    @ApiOperation(value = "Create a new city.", notes = "Gets data asynchronously")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request", response = ExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
            @ApiResponse(code = 409, message = "Conflict", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ExceptionResponse.class)})
    public ResponseEntity createCity(@RequestBody City city) {
        cityService.createCity(city);
        return ResponseEntity.ok().build();
    }

    @PutMapping(path = "/favoriteCity/{cityName}")
    @ApiOperation(value = "Add a city to users favorite list.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request", response = ExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
            @ApiResponse(code = 409, message = "Conflict", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ExceptionResponse.class)})
    public ResponseEntity updateCity(@PathVariable("cityName") String cityName) {
        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        cityService.addCityToFavorites(email, cityName);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/favoriteCity/{cityName}")
    @ApiOperation("Remove a city from the users favorite list.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request", response = ExceptionResponse.class),
            @ApiResponse(code = 403, message = "Forbidden", response = ExceptionResponse.class),
            @ApiResponse(code = 404, message = "Not found", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ExceptionResponse.class)})
    public ResponseEntity deleteCity(@PathVariable("cityName") String cityName) {
        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        cityService.removeCityFromFavorites(email, cityName);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/cities")
    @ApiOperation("Fetch unsorted city list.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ExceptionResponse.class)})
    public ResponseEntity fetchCities() {
        return ResponseEntity.ok(cityService.fetchAllCities());
    }

    @GetMapping(path = "/cities/latest")
    @ApiOperation("Fetch city list sorted by date.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ExceptionResponse.class)})
    public ResponseEntity fetchLatestCities() {
        return ResponseEntity.ok(cityService.fetchAllCitiesSortedByDate());
    }

    @GetMapping(path = "/cities/favorites")
    @ApiOperation("Fetch city list sorted by users favorite.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Bad request", response = ExceptionResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ExceptionResponse.class)})
    public ResponseEntity fetchFavoritesCities() {
        return ResponseEntity.ok(cityService.fetchAllCitiesByFavorites());
    }
}
