package hr.com.city.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Entity model that represents a favorite row in the favorite cities table.
 */
@Entity
@Getter
@Setter
@Table(name = "FAVORITE_CITIES")
@EqualsAndHashCode(exclude = "id")
public class Favorites {

    public Favorites() {
    }

    public Favorites(@NotEmpty String email, @NotEmpty String cityName) {
        this.email = email;
        this.cityName = cityName;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FC_ID_PK")
    private Long id;

    @NotEmpty
    @Column(name = "FC_EMAIL")
    private String email;

    @NotEmpty
    @Column(name = "FC_NAME")
    private String cityName;
}
