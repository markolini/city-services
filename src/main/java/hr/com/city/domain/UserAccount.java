package hr.com.city.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Entity model that represents a single user account.
 */
@Entity
@Getter
@Setter
@Table(name = "USERS")
@ToString(exclude = "password")
public class UserAccount {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USR_ID_PK")
    private Long id;

    @NotEmpty
    @Column(name = "USR_EMAIL")
    private String email;

    @NotEmpty
    @Column(name = "USR_PASSWORD")
    private String password;
}
