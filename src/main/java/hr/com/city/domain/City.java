package hr.com.city.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

/**
 * Entity model that represents a single city.
 */
@Entity
@Getter
@Setter
@Table(name = "CITIES")
public class City {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CTS_ID_PK")
    private Long id;

    @NotEmpty
    @Column(name = "CTS_NAME")
    private String name;

    @NotEmpty
    @Column(name = "CTS_DESCRIPTION")
    private String description;

    @NotEmpty
    @Column(name = "CTS_POPULATION")
    private String population;

    @JsonIgnore
    @Column(name = "CTS_CREATED")
    @Temporal(TemporalType.DATE)
    @CreatedDate
    private Date created;

    @JsonIgnore
    @Column(name = "CTS_FAVORITE_COUNTER")
    private Integer favoriteCount;
}
