package hr.com.city.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger 2 configuration for the exposed REST APIs.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private static final String API_NAME = "City Application REST API";

    @Bean
    public Docket applicationApi() {
        Docket appDocket = new Docket(DocumentationType.SWAGGER_2);
        appDocket.useDefaultResponseMessages(false);
        appDocket.groupName(API_NAME);
        appDocket.apiInfo(applicationApiInfo());
        appDocket.select().apis(RequestHandlerSelectors.basePackage("hr.com.city.api")).build();
        return appDocket;
    }

    private ApiInfo applicationApiInfo() {
        return new ApiInfoBuilder().title(API_NAME).version("1.0.0").build();
    }
}
