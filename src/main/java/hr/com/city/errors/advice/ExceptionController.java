package hr.com.city.errors.advice;

import hr.com.city.errors.exceptions.CityAlreadyExistsException;
import hr.com.city.errors.exceptions.CityDoesNotExistException;
import hr.com.city.errors.exceptions.ExceptionResponse;
import hr.com.city.errors.exceptions.FavoritesEmptyException;
import hr.com.city.errors.exceptions.UserAlreadyExistsException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Advice with a list of all exceptions that need to be handled on the controller level.
 */
@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(value = UserAlreadyExistsException.class)
    public ResponseEntity<ExceptionResponse> userExistsHandler(UserAlreadyExistsException exc) {
        return new ResponseEntity<>(createConflictResponse(exc.getMessage()), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = CityAlreadyExistsException.class)
    public ResponseEntity<ExceptionResponse> cityExistsHandler(CityAlreadyExistsException exc) {
        return new ResponseEntity<>(createConflictResponse(exc.getMessage()), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = CityDoesNotExistException.class)
    public ResponseEntity<ExceptionResponse> nonExistentCityHandler(CityDoesNotExistException exc) {
        return new ResponseEntity<>(createNotFoundResponse(exc.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = FavoritesEmptyException.class)
    public ResponseEntity<ExceptionResponse> favoriteListEmptyHandler(FavoritesEmptyException exc) {
        return new ResponseEntity<>(createNotFoundResponse(exc.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ExceptionResponse> generalExceptionHandler(Exception exc) {
        return new ResponseEntity<>(createInternalServerErrorResponse(exc.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = UsernameNotFoundException.class)
    public ResponseEntity<ExceptionResponse> userNotFoundHandler(UsernameNotFoundException exc) {
        return new ResponseEntity<>(createNotFoundResponse(exc.getMessage()), HttpStatus.NOT_FOUND);
    }

    private ExceptionResponse createConflictResponse(String message) {
        return createResponse(HttpStatus.CONFLICT.value(), message);
    }

    private ExceptionResponse createInternalServerErrorResponse(String message) {
        return createResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), message);
    }

    private ExceptionResponse createNotFoundResponse(String message) {
        return createResponse(HttpStatus.NOT_FOUND.value(), message);
    }

    private ExceptionResponse createResponse(int code, String message) {
        return new ExceptionResponse(code, message);
    }
}
