package hr.com.city.errors.exceptions;

/**
 * Exception used in case of the given city does not exist in the database or the favorites list.
 */
public class CityDoesNotExistException extends RuntimeException {

    public CityDoesNotExistException() {
    }

    public CityDoesNotExistException(String message) {
        super(message);
    }

    public CityDoesNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public CityDoesNotExistException(Throwable cause) {
        super(cause);
    }

    public CityDoesNotExistException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
