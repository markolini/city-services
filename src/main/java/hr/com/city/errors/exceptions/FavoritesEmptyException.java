package hr.com.city.errors.exceptions;

/**
 * Exception used in case of empty favorites list.
 */
public class FavoritesEmptyException extends RuntimeException {

    public FavoritesEmptyException() {
    }

    public FavoritesEmptyException(String message) {
        super(message);
    }

    public FavoritesEmptyException(String message, Throwable cause) {
        super(message, cause);
    }

    public FavoritesEmptyException(Throwable cause) {
        super(cause);
    }

    public FavoritesEmptyException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
