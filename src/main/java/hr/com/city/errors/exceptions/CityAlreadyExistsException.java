package hr.com.city.errors.exceptions;

/**
 * Exception used in case of the given city already exists in the database or the users favorites list.
 */
public class CityAlreadyExistsException extends RuntimeException {

    public CityAlreadyExistsException() {
    }

    public CityAlreadyExistsException(String message) {
        super(message);
    }

    public CityAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public CityAlreadyExistsException(Throwable cause) {
        super(cause);
    }

    public CityAlreadyExistsException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
