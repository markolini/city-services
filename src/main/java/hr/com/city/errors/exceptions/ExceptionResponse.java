package hr.com.city.errors.exceptions;

import java.io.Serializable;
import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

/**
 * General response with error code and description. Used with most exception throws.
 */
@Getter
@Setter
public class ExceptionResponse implements Serializable {

    public ExceptionResponse(@NotEmpty int code, @NotEmpty String description) {
        this.code = code;
        this.description = description;
    }

    @NotEmpty
    private int code;

    @NotEmpty
    private String description;
}
