package hr.com.city.test.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hr.com.city.api.CityController;
import hr.com.city.domain.City;
import hr.com.city.service.CityService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CityController.class, secure = false)
public class CityControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CityService cityService;

    @Test
    public void createCityTest() throws Exception {
        String requestJson = mapToJson(createTeslaCity());

        mockMvc.perform(post("/city")
                .content(requestJson)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void addCityToFavoritesTest() throws Exception {
        String URI = "/favoriteCity/{cityName}";
        String cityName = "Tesla City";

        RequestBuilder builder = MockMvcRequestBuilders
                .put(URI, cityName)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(builder).andExpect(status().is5xxServerError());
    }

    @Test
    public void removeCityFromFavoritesTest() throws Exception {
        String URI = "/favoriteCity/{cityName}";
        String email = "test@test.hr";
        String cityName = "Tesla City";

        RequestBuilder builder = MockMvcRequestBuilders
                .put(URI, cityName)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        mockMvc.perform(builder).andExpect(status().is5xxServerError());
    }

    @Test
    public void citiesFetchTest() throws Exception {
        String URI = "/cities";
        String inputJson = mapToJson(createCities());

        Mockito.when(cityService.fetchAllCities()).thenReturn(Optional.of(createCities()));

        RequestBuilder builder = MockMvcRequestBuilders
                .get(URI)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(builder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        String outputJson = response.getContentAsString();
        assertThat(outputJson).isEqualTo(inputJson);
    }

    @Test
    public void dateOrderCitiesFetch() throws Exception {
        String URI = "/cities/latest";
        String inputJson = mapToJson(createCities());

        Mockito.when(cityService.fetchAllCitiesSortedByDate()).thenReturn(Optional.of(createCities()));

        RequestBuilder builder = MockMvcRequestBuilders
                .get(URI)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(builder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        String outputJson = response.getContentAsString();
        assertThat(outputJson).isEqualTo(inputJson);
    }

    @Test
    public void favoriteOrderCitiesFetch() throws Exception {
        String URI = "/cities/favorites";
        String inputJson = mapToJson(createCities());

        Mockito.when(cityService.fetchAllCitiesByFavorites()).thenReturn(Optional.of(createCities()));

        RequestBuilder builder = MockMvcRequestBuilders
                .get(URI)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(builder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        String outputJson = response.getContentAsString();
        assertThat(outputJson).isEqualTo(inputJson);
    }

    private City createTeslaCity() {
        final City city = new City();

        city.setName("Tesla City");
        city.setDescription("Shockingly good");
        city.setPopulation("10071856");
        city.setCreated(new Date());
        city.setFavoriteCount(15);

        return city;
    }

    private City createDummyCity() {
        final City city = new City();

        city.setName("Dummy");
        city.setDescription("Dummy");
        city.setPopulation("0");
        city.setCreated(new Date());
        city.setFavoriteCount(50);

        return city;
    }

    private City createTexasCity() {
        final City city = new City();

        city.setName("Texas");
        city.setDescription("Murrrrica");
        city.setPopulation("1 000 000");
        city.setCreated(new Date());
        city.setFavoriteCount(25);

        return city;
    }

    private List<City> createCities() {
        final List<City> cities = new ArrayList<>();

        cities.add(createTeslaCity());
        cities.add(createTexasCity());
        cities.add(createDummyCity());

        return cities;
    }

    private String mapToJson(Object value) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(value);
    }
}
