package hr.com.city.test.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hr.com.city.api.UserController;
import hr.com.city.domain.UserAccount;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(value = UserController.class, secure = false)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserController userService;

    @Test
    public void registerUserTest() throws Exception {
        String requestJson = mapToJson(createUser());

        mockMvc.perform(post("/register")
                .content(requestJson)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void userLoginTest() throws Exception {
        String requestJson = mapToJson(createUser());

        mockMvc.perform(post("/login")
                .content(requestJson)
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    private UserAccount createUser() {
        final UserAccount user = new UserAccount();

        user.setEmail("test@test.hr");
        user.setPassword("test123");

        return user;
    }

    private String mapToJson(Object value) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(value);
    }
}
