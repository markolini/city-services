package hr.com.city.test.service;

import static org.assertj.core.api.Assertions.assertThat;

import hr.com.city.dao.UserDao;
import hr.com.city.domain.UserAccount;
import hr.com.city.errors.exceptions.UserAlreadyExistsException;
import hr.com.city.service.UserService;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserDao userRepository;

    @Test
    public void createUserFailTest() {
        String email = "test@test.hr";

        Mockito.when(userRepository.findByEmail(email)).thenReturn(Optional.of(createTestUser()));
        try {
            userService.registerUser(createTestUser());
        } catch (UserAlreadyExistsException exc) {
            assertThat(exc).isExactlyInstanceOf(UserAlreadyExistsException.class);
        }
    }

    @Test
    public void createUserSuccessTest() {
        String email = "buraz@test.hr";

        Mockito.when(userRepository.findByEmail(email)).thenReturn(Optional.empty());
        try {
            userService.registerUser(createBrotherUser());
        } catch (UserAlreadyExistsException exc) {
            assertThat(exc).doesNotThrowAnyException();
        }
    }

    public UserAccount createTestUser() {
        final UserAccount user = new UserAccount();

        user.setEmail("test@test.hr");
        user.setPassword("test123");

        return user;
    }

    public UserAccount createBrotherUser() {
        final UserAccount user = new UserAccount();

        user.setEmail("buraz@test.hr");
        user.setPassword("test456");

        return user;
    }
}
