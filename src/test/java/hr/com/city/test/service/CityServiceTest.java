package hr.com.city.test.service;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hr.com.city.dao.CityDao;
import hr.com.city.dao.FavoritesDao;
import hr.com.city.domain.City;
import hr.com.city.domain.Favorites;
import hr.com.city.errors.exceptions.CityAlreadyExistsException;
import hr.com.city.errors.exceptions.CityDoesNotExistException;
import hr.com.city.errors.exceptions.FavoritesEmptyException;
import hr.com.city.service.CityService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CityServiceTest {

    @Autowired
    private CityService cityService;

    @MockBean
    private CityDao cityRepository;

    @MockBean
    private FavoritesDao favoritesDao;

    @Test
    public void createCityFailTest() throws Exception {
        City cityToCreate = createTeslaCity();

        Mockito.when(cityRepository.findByName(cityToCreate.getName())).thenReturn(Optional.of(cityToCreate));
        try {
            cityService.createCity(cityToCreate);
        } catch (CityAlreadyExistsException exc) {
            assertThat(exc).isExactlyInstanceOf(CityAlreadyExistsException.class);
        }
    }

    @Test
    public void createCitySuccessTest() throws Exception {
        City cityToCreate = createTeslaCity();

        Mockito.when(cityRepository.findByName(cityToCreate.getName())).thenReturn(Optional.empty());
        try {
            cityService.createCity(cityToCreate);
        } catch (CityAlreadyExistsException exc) {
            assertThat(exc).doesNotThrowAnyException();
        }
    }

    @Test
    public void addCityToFavoritesFail() throws Exception {
        String email = "test@test.hr";
        City city = createTeslaCity();
        List<Favorites> favorites = createFavorites();

        Mockito.when(favoritesDao.findByEmail(email)).thenReturn(Optional.of(favorites));
        try {
            cityService.addCityToFavorites(email, city.getName());
        } catch (CityAlreadyExistsException exc) {
            assertThat(exc).isExactlyInstanceOf(CityAlreadyExistsException.class);
        }
    }

    @Test
    public void addCityToFavoritesSuccess() throws Exception {
        String email = "test@test.hr";
        City city = createTeslaCity();

        Mockito.when(favoritesDao.findByEmail(email)).thenReturn(Optional.empty());
        try {
            cityService.addCityToFavorites(email, city.getName());
        } catch (CityAlreadyExistsException exc) {
            assertThat(exc).isExactlyInstanceOf(CityAlreadyExistsException.class);
        }
    }

    @Test
    public void removeCityFromFavoritesFailNoFavorites() throws Exception {
        String email = "test@test.hr";
        City city = createTeslaCity();

        Mockito.when(favoritesDao.findByEmail(email)).thenReturn(Optional.empty());
        try {
            cityService.removeCityFromFavorites(email, city.getName());
        } catch (FavoritesEmptyException exc) {
            assertThat(exc).isExactlyInstanceOf(FavoritesEmptyException.class);
        }
    }

    @Test
    public void removeCityFromFavoritesFailNoCity() throws Exception {
        String email = "test@test.hr";
        City nonExistingCity = createTeslaCity();
        List<Favorites> favoritesWithoutTheNeededOne = createFavoritesWithoutTesla();

        Mockito.when(favoritesDao.findByEmail(email)).thenReturn(Optional.of(favoritesWithoutTheNeededOne));
        try {
            cityService.removeCityFromFavorites(email, nonExistingCity.getName());
        } catch (CityDoesNotExistException exc) {
            assertThat(exc).isExactlyInstanceOf(CityDoesNotExistException.class);
        }
    }


    @Test
    public void removeCityFromFavoritesSuccess() throws Exception {
        String email = "test@test.hr";
    }

    @Test
    public void fetchCities() throws Exception {
        String outputJson = mapToJson(randomCities());

        Mockito.when(cityRepository.findAll()).thenReturn(randomCities());
        assertThat(mapToJson(cityService.fetchAllCities().get())).isEqualTo(outputJson);
    }

    @Test
    public void sortCitiesByDate() throws Exception {
        String outputJson = mapToJson(dateSortedCities());

        Mockito.when(cityRepository.findAll()).thenReturn(randomCities());
        assertThat(mapToJson(cityService.fetchAllCitiesSortedByDate().get())).isEqualTo(outputJson);
    }

    @Test
    public void sortCitiesByFavorites() throws Exception {
        String outputJson = mapToJson(favoritesSortedCities());

        Mockito.when(cityRepository.findAll()).thenReturn(randomCities());
        assertThat(mapToJson(cityService.fetchAllCitiesByFavorites().get())).isEqualTo(outputJson);
    }

    private City createTeslaCity() throws Exception {
        final City city = new City();

        city.setName("Tesla City");
        city.setDescription("Shockingly good");
        city.setPopulation("10071856");
        city.setCreated(createDate("2018-10-20"));
        city.setFavoriteCount(15);

        return city;
    }

    private City createDummyCity() throws Exception {
        final City city = new City();

        city.setName("Dummy");
        city.setDescription("Dummy");
        city.setPopulation("0");
        city.setCreated(createDate("2018-10-19"));
        city.setFavoriteCount(50);

        return city;
    }

    private City createTexasCity() throws Exception {
        final City city = new City();

        city.setName("Texas");
        city.setDescription("Murrrrica");
        city.setPopulation("1 000 000");
        city.setCreated(createDate("2018-10-21"));
        city.setFavoriteCount(25);

        return city;
    }

    private List<City> randomCities() throws Exception {
        final List<City> cities = new ArrayList<>();

        cities.add(createTeslaCity());
        cities.add(createTexasCity());
        cities.add(createDummyCity());

        return cities;
    }

    private List<City> dateSortedCities() throws Exception {
        final List<City> cities = new ArrayList<>();

        cities.add(createTexasCity());
        cities.add(createTeslaCity());
        cities.add(createDummyCity());

        return cities;
    }

    private List<City> favoritesSortedCities() throws Exception {
        final List<City> cities = new ArrayList<>();

        cities.add(createDummyCity());
        cities.add(createTexasCity());
        cities.add(createTeslaCity());

        return cities;
    }

    private Date createDate(String dateToCreate) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = dateToCreate;
        Date date = format.parse(strDate);
        return date;
    }

    private String mapToJson(Object value) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(value);
    }

    private Favorites createTeslaFavorite() {
        final Favorites favorite = new Favorites();

        favorite.setId(123456789L);
        favorite.setEmail("test@test.hr");
        favorite.setCityName("Tesla City");

        return favorite;
    }

    private Favorites createTexasFavorite() {
        final Favorites favorite = new Favorites();

        favorite.setId(1236546789L);
        favorite.setEmail("test@test.hr");
        favorite.setCityName("Texas City");

        return favorite;
    }

    private Favorites createDummyFavorite() {
        final Favorites favorite = new Favorites();

        favorite.setId(321456789L);
        favorite.setEmail("test@test.hr");
        favorite.setCityName("Dummy");

        return favorite;
    }

    private List<Favorites> createFavorites() {
        final List<Favorites> favorites = new ArrayList<>();

        favorites.add(createTeslaFavorite());
        favorites.add(createDummyFavorite());
        favorites.add(createTexasFavorite());

        return favorites;
    }

    private List<Favorites> createFavoritesWithoutTesla() {
        final List<Favorites> favorites = new ArrayList<>();

        favorites.add(createDummyFavorite());
        favorites.add(createTexasFavorite());

        return favorites;
    }
}
