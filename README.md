# City Services Application

Application used to book your favorite cities. :)

## What is this all about

The project has is a single application with two exposed REST API layers, business logic middle layer and H2 in memory database. JWT layer 
is used for user authentication and authorization.

### Architecture

The service application is bundled in a single jar file.

1. REST API layer, accepting and handling REST API calls
2. Service middle layer with business logic
3. JWT layer for authentication and authorization
4. H2 in memory database with JPA and Hibernate support

## Built With

* [Java 8](https://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html) - Java programming language
* [H2](http://www.h2database.com/) - H2 in memory database
* [Spring Boot](http://spring.io/projects/spring-boot) - Spring Framework, Spring Boot, Spring Security
* [Hibernate](http://hibernate.org/) - JPA and Hibernate
* [JWT](https://github.com/jwtk/jjwt) - Json Web Token library
* [Lombok](https://projectlombok.org/) - Lombok
* [Swagger](https://swagger.io/) - SpringFox Swagger 2
* [JUnit](https://junit.org) - JUnit test suit
* [Maven](https://maven.apache.org/) - Maven

### Prerequisites

It requires installed Java 8 and Maven.

### Installation

Check out the project
```
git clone https://gitlab.com/markolini/user-numbers.git
```

pack with maven
```
mvn package
```

and run the jar
```
java -jar city-services-1.0.0.jar
```

or run from your favorite IDE.

## Getting Started

The Swagger page is located at URL:
```
http://localhost:8080/swagger-ui.html
```

The H2 console page is located at URL:
```
http://localhost:8080/console
```

Enjoy. :)
